#-*-coding:utf8;-*-
#qpy:2
#qpy:kivy
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.modalview import ModalView
from kivy.uix.textinput import TextInput
from kivy.uix.actionbar import ActionBar 
from kivy.uix.actionbar import ActionView
from kivy.uix.actionbar import ActionButton
from kivy.uix.actionbar import ActionPrevious
from kivy.uix.tabbedpanel import TabbedPanel
from time import sleep
from kivy.clock import Clock
from kivy.network.urlrequest import UrlRequest
from kivy.logger import Logger
import random

class MyWidget(BoxLayout):
    def __init__(self, **kwargs):
        self.start =None
        self.listmob = [] 
        self.listobj ="Please before scan"
        self.buffers=""
        super(MyWidget, self).__init__(**kwargs)
        self.create_toolbar()
        #self.add_widget(Label(text="press for test", size_hint=(.10,.3) ))
        but = Button(text="Scaning list", size_hint=(.5,.1) )
        self.add_widget(but) 
        but.bind(on_press=self.show_pop)
        stop = Button(text="Stop", size_hint=(.5,.1) )
        stop.bind(on_press=self.stop_scan)
        self.add_widget(stop)
        but_run =Button(text="Run", size_hint=(.5,.1))
        but_run.bind(on_press=self.run_scan)
        but_ex= Button(text="Script run", size_hint=(.5,.1)) 
        self.add_widget(but_run)
        but_ex.bind(on_press=self.test_network)
        self.add_widget(but_ex)
        self.l = Label(text="", size_hint=(.9,.3) ) 
        self.add_widget(self.l)
        table = TabbedPanel() 
        #self.add_widget(table)
               
        Clock.schedule_once(self.boom2,5)
        
    def run_scan(self, instance):
        self.start=Clock.schedule_interval(self.boom,0.1)
    
    def stop_scan(self,instance):
        self.start.cancel()
        
    def on_return_ok(self,req, result):
        Logger.info(result)
        
    def test_network(self,instance):
       url="https://inbox.lv"
       req = UrlRequest(url,self.on_return_ok) 
        
    def boom(self, dt):
       r =  random.random()
       self.add_to_list(r)
       self.listobj= str(self.return_last(15)) 
       self.l.text="Scaning device 0x00"+str(r)
       
    def add_to_list(self,obj):
        self.listmob.append(obj)
        
    def return_last(self,minobj):
         try:
             return self.listmob[0:15]
         except IndexError:
             return "" 
    
       
       
    def boom2(self, dt):
       but =Button(text="Boom", size_hint=(.2,.2)) 
       #self.add_widget(but)
        
    def create_toolbar(self):
        bar = ActionBar() 
        menu = ActionView(action_previous=ActionPrevious()) 
        bar.add_widget(menu)
        menu.add_widget(ActionButton(text="Scaner"))
        menu.add_widget(ActionButton(text="Tools"))
        menu.add_widget(ActionButton(text="Exploit"))
        menu.add_widget(ActionButton(text="Command"))
        about = ActionButton(text="About") 
        about.bind(on_press=self.about_pop)
        menu.add_widget(about)
        self.add_widget(bar)
        
        
    def show_pop(self,instance):
        message="Scaning list"
        self.field= TextInput(text=self.listobj,size_hint=(.7,.6))
        but = Button(text="close", size_hint_y=None, width=100) 
        layoutbox= GridLayout(rows=2 ) 
        layoutbox.add_widget(self.field)
        layoutbox.add_widget(but)
        pop = Popup(auto_dismiss=False, size_hint=(.7,.7) ) 
        pop.title="Scanimg"
        pop.add_widget(layoutbox )
        
        but.bind(on_press=pop.dismiss)
        pop.open()
    
    def show_pop2(self,instance):
        message="Run commands"
        content=TextInput(text="", size_hint=(.2,.2)) 
        content2=Button(text="close", size_hint=(.2,.2) ) 
        widgets= BoxLayout(position="vertical") 
        widgets.add_widget(content)
        text = Button(text="close", size_hint=(.1,.2) ) 
        widgets.add_widget(text)
        pop = Popup(title=message, content=widgets, auto_dismiss=False, size_hint=(.9,.9)) 
        text.bind(on_press=pop.dismiss)
        pop.open()
        
    def show_lebel(self, text) :
        label=Label(text=text) 
        self.add_widget(label)
        
    def about_pop(self, instance ):
        message="About"
        self.field= Label(text="created by clown ",size_hint=(.7,.8))
        but = Button(text="close",size_hint=(.2,.2)) 
        layoutbox= GridLayout(rows=2 ) 
        layoutbox.add_widget(self.field)
        layoutbox.add_widget(but)
        pop = Popup(title=message, content=layoutbox, auto_dismiss=False, size_hint=(.9,.9)) 
        but.bind(on_press=pop.dismiss)
        pop.open()
       
       
        
 
class TestApp(App):
    def build(self):
        # display a button with the text : Hello QPython 
        return MyWidget(spacing=10, orientation="vertical") 

TestApp().run()

